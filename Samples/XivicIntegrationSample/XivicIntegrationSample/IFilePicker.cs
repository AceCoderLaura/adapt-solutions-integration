﻿using System.IO;
using System.Threading.Tasks;

namespace XivicIntegrationSample
{
    public interface IFilePicker
    {
        Task<Stream> PickFile();
        string FileName { get; }
    }
}
