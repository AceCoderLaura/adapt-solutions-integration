﻿namespace XivicIntegrationSample.Model
{
    /// <summary>
    /// The response is the Id of the record saved or created unless there was an error during the save operation
    /// </summary>
    public class SaveJobResponse : XivicResponseBase<object>
    {
    }
}