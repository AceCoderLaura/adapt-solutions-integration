﻿namespace XivicIntegrationSample.Model
{
    public class GetJobRequest : XivicRequestBase
    {
        public object EntityId { get; set; }
    }
}
