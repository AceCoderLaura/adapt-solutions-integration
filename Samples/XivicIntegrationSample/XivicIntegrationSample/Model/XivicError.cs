﻿namespace XivicIntegrationSample.Model
{
    /// <summary>
    /// An error returned from Xivic when a Save, or Get operation failed.
    /// </summary>
    public class XivicError
    {
        public XivicError()
        {

        }

        public XivicError(string message)
        {
            Message = message;
        }

        public XivicError(string message, string fieldName) : this(message)
        {
            FieldName = fieldName;
        }

        public string FieldName { get; set; }
        public string Message { get; private set; }
    }

}
