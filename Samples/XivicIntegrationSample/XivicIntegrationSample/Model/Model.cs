﻿namespace XivicIntegrationSample.Model
{
    public class CredentialsModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class AuthenticationModel
    {
        public bool IsSuccess { get; set; }
        public string Authtoken { get; set; }
        public int UserKey { get; set; }
    }
}
