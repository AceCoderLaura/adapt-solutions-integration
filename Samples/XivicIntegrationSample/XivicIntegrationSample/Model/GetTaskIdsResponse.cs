﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace XivicIntegrationSample.Model
{
    public class GetTaskIdsResponse 
    {
        public Collection<XivicError> Errors { get; } = new Collection<XivicError>();
        public List<int> Result { get; set; }
    }
}
