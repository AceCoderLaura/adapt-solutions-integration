﻿using System.Collections.Generic;

namespace XivicIntegrationSample.Model
{
    public class JobNote
    {
        public string NoteId { get; set; }
        public string NoteText { get; set; }
        public string NoteType { get; set; }
    }

    public class Job
    {
        public int JobNumber { get; set; }
        public string ProblemDescription { get; set; }
        public string Asset { get; set; }
        public List<JobNote> JobNotes { get; set; }
    }
}
