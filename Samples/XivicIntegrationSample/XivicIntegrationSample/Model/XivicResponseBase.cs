﻿using System.Collections.ObjectModel;

namespace XivicIntegrationSample.Model
{
    public class XivicResponseBase<T> : XivicResponseBase
    {
        public T Result { get; set; }
    }

    public class XivicResponseBase 
    {
        public Collection<XivicError> Errors { get; } = new Collection<XivicError>();
    }

}
