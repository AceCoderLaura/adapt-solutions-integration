﻿

namespace XivicIntegrationSample.Model
{
    public class SaveJobRequest : XivicRequestBase
    {
        public Job Entity { get; set; }
    }
}