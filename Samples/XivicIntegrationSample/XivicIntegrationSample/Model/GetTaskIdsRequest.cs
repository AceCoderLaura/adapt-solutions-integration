﻿using System;

namespace XivicIntegrationSample.Model
{
    public class GetTaskIdsRequest 
    {
        public string AuthToken { get; set; }
        public DateTime? EditDateStart { get; set; }
        public DateTime? EditDateEnd { get; set; }
        public DateTime? ProblemDateStart { get; set; }
        public DateTime? ProblemDateEnd { get; set; }
        public DateTime? CompletionDeadlineStart { get; set; }
        public DateTime? CompletionDeadlineEnd { get; set; }
        public int? Limit { get; set; }
        public bool IncludeClosed { get; set; }
    }
}
