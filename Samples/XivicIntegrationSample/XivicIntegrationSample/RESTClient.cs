﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace XivicIntegrationSample
{
    public class RESTClient
    {
        #region Fields
        private readonly HttpClient _HttpClient = new HttpClient();
        #endregion

        #region Public Properties
        public Uri BaseUri => _HttpClient.BaseAddress;
        public Dictionary<string, string> Headers { get; private set; } = new Dictionary<string, string>();
        #endregion

        #region Constructor
        public RESTClient(Uri baseUri)
        {
            _HttpClient.BaseAddress = baseUri;
            _HttpClient.Timeout = new TimeSpan(0, 3, 0);
        }
        #endregion

        #region Private Methods
        private async Task<T> Call<T>(string queryString, bool isPost, string contentType, object body = null)
        {
            _HttpClient.DefaultRequestHeaders.Clear();

            string json;
            HttpResponseMessage result = null;
            if (!isPost)
            {
                _HttpClient.DefaultRequestHeaders.Clear();
                foreach (var key in Headers.Keys)
                {
                    _HttpClient.DefaultRequestHeaders.Add(key, Headers[key]);
                }

                result = await _HttpClient.GetAsync(queryString);
            }
            else
            {
                var bodyString = body != null ? JsonConvert.SerializeObject(body) : string.Empty;
                var stringContent = new StringContent(bodyString, Encoding.UTF8, contentType);

                //Don't know why but this has to be set again, otherwise more text is added on to the Content-Type header...
                stringContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);

                stringContent.Headers.ContentLength = bodyString.Length;

                foreach (var key in Headers.Keys)
                {
                    stringContent.Headers.Add(key, Headers[key]);
                }

                result = await _HttpClient.PostAsync(queryString, stringContent);

                System.Diagnostics.Debug.WriteLine($"{_HttpClient.BaseAddress.ToString()} OK");

            }

            if (result.IsSuccessStatusCode)
            {
                //TODO: We could support binary here but probably not
                json = await result.Content.ReadAsStringAsync();

                if (typeof(T) == typeof(string))
                {
                    //Just return the string
                    object jsonAsObject = json;
                    return (T)jsonAsObject;
                }

                return JsonConvert.DeserializeObject<T>(json);
            }
            else
            {
                var text = await result.Content.ReadAsStringAsync();

                System.Diagnostics.Debug.WriteLine($"{_HttpClient.BaseAddress.ToString()} Error: {text}");

                throw new Exception($"Nope {result.StatusCode}.\r\nBase Uri: {_HttpClient.BaseAddress}. Full Uri: {_HttpClient.BaseAddress + queryString}\r\nError:\r\n{text}");
            }

        }
        #endregion

        #region Public Methods
        public async Task<T> GetAsync<T>(string queryString, string contentType = "application/json")
        {
            return await Call<T>(queryString, false, contentType, null);
        }

        public async Task<TReturn> PostAsync<TReturn, TBody>(TBody body, string queryString, string contentType = "application/json")
        {
            return await Call<TReturn>(queryString, true, contentType, body);
        }
        #endregion
    }


}
