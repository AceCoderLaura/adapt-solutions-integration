﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Xamarin.Forms;
using XivicIntegrationSample.Model;

namespace XivicIntegrationSample
{
    public partial class MainPage
    {
        #region Fields
        private RESTClient _XivicClient;
        private const string BasicJobMapName = "BasicJobMap";
        #endregion

        #region Private Properties
        private XivicNSwag.Job Job => BindingContext as XivicNSwag.Job;
        #endregion

        #region Constructror
        public MainPage()
        {
            InitializeComponent();
            _XivicClient = new RESTClient(new Uri(XivicUrlBox.Text));
            LoginButton.Clicked += LoginButton_Clicked;
        }
        #endregion

        #region Event Handlers
        private async void LoginButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                SetIsBusy(true);
                var authModel = await _XivicClient.PostAsync<AuthenticationModel, CredentialsModel>(new CredentialsModel { Username = UsernameBox.Text, Password = ThePasswordBox.Text }, "authenticate");
                if (authModel.IsSuccess)
                {
                    AuthtokenBox.Text = authModel.Authtoken;
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            SetIsBusy(false);
        }

        private async void GetJobButton_Clicked(object sender, EventArgs e)
        {
            SetIsBusy(true);

            try
            {
                if (string.IsNullOrEmpty(AuthtokenBox.Text))
                {
                    throw new Exception("You must log in first");
                }

                var request = new XivicNSwag.GetEntityRequest { AuthToken = AuthtokenBox.Text, EntityId = int.Parse(JobNumberBox.Text) };
                var response = await new XivicNSwag.Client().GetJobAsync(request);
                if (response.Errors.Count > 0)
                {
                    throw new Exception(GetErrorMessage(response.Errors));
                }

                BindingContext = response.Result;
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            SetIsBusy(false);
        }


        private async void SaveJobButton_Clicked(object sender, EventArgs e)
        {
            SetIsBusy(true);

            try
            {
                var request = new XivicNSwag.SaveJobRequest { AuthToken = AuthtokenBox.Text, Entity = Job };
                //var response = await _XivicClient.PostAsync<SaveJobResponse, SaveJobRequest>(, "savejob");
                var response = await new XivicNSwag.Client().SaveJobAsync(request);
                if (response.Errors.Count == 0)
                {
                    await DisplayAlert("Success", "Job Saved in Xivic", "OK");
                }
                else
                {
                    throw new Exception(GetErrorMessage(null));
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            SetIsBusy(false);
        }


        private async void ListJobIds_Clicked(object sender, EventArgs e)
        {
            SetIsBusy(true);

            try
            {
                var body = new GetTaskIdsRequest { AuthToken = AuthtokenBox.Text, Limit = 10 };
                //body.EditDateStart = new DateTime(2018, 1, 8);
                //body.IncludeClosed = true;
                //body.EditDateEnd = new DateTime(2018, 1, 11);

                var response = await _XivicClient.PostAsync<GetTaskIdsResponse, GetTaskIdsRequest>(body, "GetTaskIds");
                if (response.Errors.Count == 0)
                {
                    await DisplayAlert("Success", string.Join(",", response.Result), "OK");
                }
                else
                {
                    throw new Exception(GetErrorMessage(null));
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            SetIsBusy(false);
        }

        private async void AttachFileToJob_Clicked(object sender, EventArgs e)
        {
            SetIsBusy(true);

            try
            {
                var filePicker = DependencyService.Get<IFilePicker>();
                using (var stream = await filePicker.PickFile())
                {
                    var client = new XivicNSwag.Client();
                    var result = await client.ApiAttachmentsAttachFilePostAsync(BasicJobMapName, JobNumberBox.Text, filePicker.FileName, new XivicNSwag.FileParameter(stream, filePicker.FileName), AuthtokenBox.Text);
                    Console.WriteLine(result.ToJson());
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
            }

            SetIsBusy(false);
        }

        #endregion

        #region Private Methods
        private void SetIsBusy(bool isBusy)
        {
            ActivityIndicator.IsRunning = isBusy;
            ActivityIndicator.IsVisible = isBusy;
        }

        private static string GetErrorMessage(Collection<XivicNSwag.XivicError> errors)
        {
            return string.Join("\r\n", errors.Select(er => $"{er.FieldName}: {er.Message}"));
        }

        #endregion
    }
}
