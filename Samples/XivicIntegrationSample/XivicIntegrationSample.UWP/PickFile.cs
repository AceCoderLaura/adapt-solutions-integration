﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XivicIntegrationSample;
using XivicIntegrationSample.UWP;

[assembly: Xamarin.Forms.Dependency(typeof(FilePicker))]

namespace XivicIntegrationSample.UWP
{
    class FilePicker : IFilePicker
    {
        public async Task<Stream> PickFile()
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker() { FileTypeFilter = { ".bmp", ".jpg", ".txt", ".png" } };
            var file = await picker.PickSingleFileAsync();
            FileName = file.Name;
            return await file.OpenStreamForReadAsync();
        }

        public string FileName { get; set; }
    }
}
